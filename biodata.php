<!DOCTYPE html>
<html>
<head>
<title>KTP</title>
</head>
<?php
 include "ktp.php" ;
 $manusia = new manusia();
 
 ?>
<body style="background-color: #C0C0C0 ">
	
	<table width="745" border="1" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<h1 align="center">KARTU TANDA PENDUDUK</h1>
			<h2 align="center">PROVINSI BANTEN</h2>
		</tr>
	<tr align="center" bgcolor="	#808080">
	<td width="174">DATA DIRI</td>
	<td width="353">KETERANGAN</td>
</tr>
	<tr>
	<td>NIK</td>
	<td><?php echo $manusia->tampilkan_nik(); ?></td>
	</tr>

<tr>
	<td>Nama</td>
	<td><?php echo $manusia->tampilkan_nama(); ?></td>
</tr>

<tr>
	<td>Tempat Lahir / Tgl Lahir</td>
	<td><?php echo $manusia->tampilkan_ttl(); ?></td>
</tr>

<tr>
	<td>Jenis Kelamin</td>
	<td><?php echo $manusia->tampilkan_jenkel(); ?></td>
</tr>

<tr>
	<td>Golongan Darah</td>
	<td><?php echo $manusia->tampilkan_gol(); ?></td>
</tr>

<tr>
	<td>Alamat</td>
	<td><?php echo $manusia->tampilkan_alamat(); ?></td>
</tr>

<tr>
	<td>Agama</td>
	<td><?php echo $manusia->tampilkan_agama(); ?></td>
</tr>

<tr>
	<td>Status Perkawinan</td>
	<td><?php echo $manusia->tampilkan_status(); ?></td>

</tr>

<tr>
	<td>Pekerjaan</td>
	<td><?php echo $manusia->tampilkan_pekerjaan(); ?></td>
</tr>

<tr>
	<td>Kewarganegaraan</td>
	<td><?php echo $manusia->tampilkan_kewarganegaraan(); ?></td>
</tr>

<tr>
	<td>Masa Berlaku</td>
	<td><?php echo $manusia->tampilkan_berlaku(); ?></td>
</tr>
</table>
</body>
</html>
